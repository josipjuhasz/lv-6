﻿namespace WindowsFormsApp7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nula = new System.Windows.Forms.Button();
            this.tocka = new System.Windows.Forms.Button();
            this.jednako = new System.Windows.Forms.Button();
            this.jedan = new System.Windows.Forms.Button();
            this.dva = new System.Windows.Forms.Button();
            this.tri = new System.Windows.Forms.Button();
            this.cetiri = new System.Windows.Forms.Button();
            this.pet = new System.Windows.Forms.Button();
            this.sest = new System.Windows.Forms.Button();
            this.sedam = new System.Windows.Forms.Button();
            this.osam = new System.Windows.Forms.Button();
            this.devet = new System.Windows.Forms.Button();
            this.podj = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.Button();
            this.tan = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.sin = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.text1 = new System.Windows.Forms.TextBox();
            this.clean = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nula
            // 
            this.nula.Location = new System.Drawing.Point(34, 287);
            this.nula.Name = "nula";
            this.nula.Size = new System.Drawing.Size(44, 45);
            this.nula.TabIndex = 0;
            this.nula.Text = "0";
            this.nula.UseVisualStyleBackColor = true;
            this.nula.Click += new System.EventHandler(this.nula_Click);
            // 
            // tocka
            // 
            this.tocka.Location = new System.Drawing.Point(84, 287);
            this.tocka.Name = "tocka";
            this.tocka.Size = new System.Drawing.Size(44, 45);
            this.tocka.TabIndex = 1;
            this.tocka.Text = ",";
            this.tocka.UseVisualStyleBackColor = true;
            this.tocka.Click += new System.EventHandler(this.tocka_Click);
            // 
            // jednako
            // 
            this.jednako.Location = new System.Drawing.Point(134, 287);
            this.jednako.Name = "jednako";
            this.jednako.Size = new System.Drawing.Size(44, 45);
            this.jednako.TabIndex = 2;
            this.jednako.Text = "=";
            this.jednako.UseVisualStyleBackColor = true;
            this.jednako.Click += new System.EventHandler(this.jednako_Click);
            // 
            // jedan
            // 
            this.jedan.Location = new System.Drawing.Point(34, 236);
            this.jedan.Name = "jedan";
            this.jedan.Size = new System.Drawing.Size(44, 45);
            this.jedan.TabIndex = 3;
            this.jedan.Text = "1";
            this.jedan.UseVisualStyleBackColor = true;
            this.jedan.Click += new System.EventHandler(this.jedan_Click);
            // 
            // dva
            // 
            this.dva.Location = new System.Drawing.Point(84, 236);
            this.dva.Name = "dva";
            this.dva.Size = new System.Drawing.Size(44, 45);
            this.dva.TabIndex = 4;
            this.dva.Text = "2";
            this.dva.UseVisualStyleBackColor = true;
            this.dva.Click += new System.EventHandler(this.dva_Click);
            // 
            // tri
            // 
            this.tri.Location = new System.Drawing.Point(134, 236);
            this.tri.Name = "tri";
            this.tri.Size = new System.Drawing.Size(44, 45);
            this.tri.TabIndex = 5;
            this.tri.Text = "3";
            this.tri.UseVisualStyleBackColor = true;
            this.tri.Click += new System.EventHandler(this.tri_Click);
            // 
            // cetiri
            // 
            this.cetiri.Location = new System.Drawing.Point(34, 185);
            this.cetiri.Name = "cetiri";
            this.cetiri.Size = new System.Drawing.Size(44, 45);
            this.cetiri.TabIndex = 6;
            this.cetiri.Text = "4";
            this.cetiri.UseVisualStyleBackColor = true;
            this.cetiri.Click += new System.EventHandler(this.cetiri_Click);
            // 
            // pet
            // 
            this.pet.Location = new System.Drawing.Point(84, 185);
            this.pet.Name = "pet";
            this.pet.Size = new System.Drawing.Size(44, 45);
            this.pet.TabIndex = 7;
            this.pet.Text = "5";
            this.pet.UseVisualStyleBackColor = true;
            this.pet.Click += new System.EventHandler(this.pet_Click);
            // 
            // sest
            // 
            this.sest.Location = new System.Drawing.Point(134, 185);
            this.sest.Name = "sest";
            this.sest.Size = new System.Drawing.Size(44, 45);
            this.sest.TabIndex = 8;
            this.sest.Text = "6";
            this.sest.UseVisualStyleBackColor = true;
            this.sest.Click += new System.EventHandler(this.sest_Click);
            // 
            // sedam
            // 
            this.sedam.Location = new System.Drawing.Point(34, 134);
            this.sedam.Name = "sedam";
            this.sedam.Size = new System.Drawing.Size(44, 45);
            this.sedam.TabIndex = 9;
            this.sedam.Text = "7";
            this.sedam.UseVisualStyleBackColor = true;
            this.sedam.Click += new System.EventHandler(this.button10_Click);
            // 
            // osam
            // 
            this.osam.Location = new System.Drawing.Point(84, 134);
            this.osam.Name = "osam";
            this.osam.Size = new System.Drawing.Size(44, 45);
            this.osam.TabIndex = 10;
            this.osam.Text = "8";
            this.osam.UseVisualStyleBackColor = true;
            this.osam.Click += new System.EventHandler(this.osam_Click);
            // 
            // devet
            // 
            this.devet.Location = new System.Drawing.Point(134, 134);
            this.devet.Name = "devet";
            this.devet.Size = new System.Drawing.Size(44, 45);
            this.devet.TabIndex = 11;
            this.devet.Text = "9";
            this.devet.UseVisualStyleBackColor = true;
            this.devet.Click += new System.EventHandler(this.devet_Click);
            // 
            // podj
            // 
            this.podj.Location = new System.Drawing.Point(184, 287);
            this.podj.Name = "podj";
            this.podj.Size = new System.Drawing.Size(44, 45);
            this.podj.TabIndex = 12;
            this.podj.Text = "/";
            this.podj.UseVisualStyleBackColor = true;
            this.podj.Click += new System.EventHandler(this.podj_Click);
            // 
            // puta
            // 
            this.puta.Location = new System.Drawing.Point(184, 236);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(44, 45);
            this.puta.TabIndex = 13;
            this.puta.Text = "*";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(184, 185);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(44, 45);
            this.minus.TabIndex = 14;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(184, 134);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(44, 45);
            this.plus.TabIndex = 15;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(234, 287);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(44, 45);
            this.log.TabIndex = 16;
            this.log.Text = "log";
            this.log.UseVisualStyleBackColor = true;
            this.log.Click += new System.EventHandler(this.log_Click);
            // 
            // tan
            // 
            this.tan.Location = new System.Drawing.Point(234, 236);
            this.tan.Name = "tan";
            this.tan.Size = new System.Drawing.Size(44, 45);
            this.tan.TabIndex = 17;
            this.tan.Text = "tan";
            this.tan.UseVisualStyleBackColor = true;
            this.tan.Click += new System.EventHandler(this.tan_Click);
            // 
            // cos
            // 
            this.cos.Location = new System.Drawing.Point(234, 185);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(44, 45);
            this.cos.TabIndex = 18;
            this.cos.Text = "cos";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.cos_Click);
            // 
            // sin
            // 
            this.sin.Location = new System.Drawing.Point(234, 134);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(44, 45);
            this.sin.TabIndex = 19;
            this.sin.Text = "sin";
            this.sin.UseVisualStyleBackColor = true;
            this.sin.Click += new System.EventHandler(this.sin_Click);
            // 
            // sqrt
            // 
            this.sqrt.Location = new System.Drawing.Point(234, 83);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(44, 45);
            this.sqrt.TabIndex = 20;
            this.sqrt.Text = "sqrt";
            this.sqrt.UseVisualStyleBackColor = true;
            this.sqrt.Click += new System.EventHandler(this.sqrt_Click);
            // 
            // text1
            // 
            this.text1.Location = new System.Drawing.Point(34, 45);
            this.text1.Name = "text1";
            this.text1.Size = new System.Drawing.Size(244, 20);
            this.text1.TabIndex = 21;
            this.text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.text1.TextChanged += new System.EventHandler(this.text1_TextChanged);
            // 
            // clean
            // 
            this.clean.Location = new System.Drawing.Point(34, 83);
            this.clean.Name = "clean";
            this.clean.Size = new System.Drawing.Size(44, 45);
            this.clean.TabIndex = 22;
            this.clean.Text = "C";
            this.clean.UseVisualStyleBackColor = true;
            this.clean.Click += new System.EventHandler(this.clean_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 342);
            this.Controls.Add(this.clean);
            this.Controls.Add(this.text1);
            this.Controls.Add(this.sqrt);
            this.Controls.Add(this.sin);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.tan);
            this.Controls.Add(this.log);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.podj);
            this.Controls.Add(this.devet);
            this.Controls.Add(this.osam);
            this.Controls.Add(this.sedam);
            this.Controls.Add(this.sest);
            this.Controls.Add(this.pet);
            this.Controls.Add(this.cetiri);
            this.Controls.Add(this.tri);
            this.Controls.Add(this.dva);
            this.Controls.Add(this.jedan);
            this.Controls.Add(this.jednako);
            this.Controls.Add(this.tocka);
            this.Controls.Add(this.nula);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button nula;
        private System.Windows.Forms.Button tocka;
        private System.Windows.Forms.Button jednako;
        private System.Windows.Forms.Button jedan;
        private System.Windows.Forms.Button dva;
        private System.Windows.Forms.Button tri;
        private System.Windows.Forms.Button cetiri;
        private System.Windows.Forms.Button pet;
        private System.Windows.Forms.Button sest;
        private System.Windows.Forms.Button sedam;
        private System.Windows.Forms.Button osam;
        private System.Windows.Forms.Button devet;
        private System.Windows.Forms.Button podj;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button log;
        private System.Windows.Forms.Button tan;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button sin;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.TextBox text1;
        private System.Windows.Forms.Button clean;
    }
}

